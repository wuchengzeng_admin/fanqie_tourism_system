<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">

	<van-row class="bg_ff photo-list cr_ff">
		<van-col span="24" class="text-center"><img v-lazy="item.thumb" ></van-col>
		<van-col span="24" class="title"><van-icon name="location icon-bottom" ></van-icon> <span class="icon-bottom" v-text="item.title" ></span></van-col>
	</van-row>
	
	<div class="bg_ff pb10 pt10">
		<waterfall :line="line" :line-gap="200" :min-line-gap="150" :max-line-gap="220" :watch="list"  ref="waterfall" >
		  <waterfall-slot class="text-center" v-for="(v, index) in list" :width="v.w" :height="v.h"  :order="index" :key="v.id" >
		   	<img v-lazy="v.thumb" style="width: 96%;">
		  </waterfall-slot>
		</waterfall>
	</div>
	<div class="mt60">&nbsp;</div>
</div>

<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/vue-waterfall.min.js'?>" ></script>
<script>
var id = <?php echo $id;?>;
new Vue({
	el: '#app',
	data: {line: 'v',
		list: '',
		item:<?php echo json_encode($item);?>
	},
	components: {
	    'waterfall': Waterfall.waterfall,
	    'waterfall-slot': Waterfall.waterfallSlot
	  },
  	methods: {
  	  	init(){
  	  	  	axios.post('/api/photo/get_lists', Qs.stringify({id:id}),ajaxconfig).then((response)=> {
	  	  	  	var data = response.data;
	  	      	if(data.state==1){
	  	  	      	this.list = data.data;
	  	  	  	}else{
					this.$toast(data.message);
	  	  	  	}
	  	    });
  	  	}
  	},
  	mounted:function (){
  		this.init()
	}
});
</script>
</body>
</html>