<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<style>
video{width: 100%;height:200px;background-color: #000;} .content p{margin-bottom: 10px;}
audio{width: 100%;}
img{width: 100%}
video::-webkit-media-controls-enclosure {overflow:hidden;}
video::-webkit-media-controls-panel {width: calc(100% + 40px);}
audio::-webkit-media-controls-enclosure {overflow:hidden;}
audio::-webkit-media-controls-panel {width: calc(100% + 40px);}
</style>
<div id="app">
	<van-row class="bg_ff">
		<van-col  span="24">
			<lable class="lable-img" style="height:210px;"><img :src="item.h_thumb" ></lable>
		</van-col>
		<van-col  span="24" class="pl10 pr10 mt5 mb10 f14">
			<span v-text="item.title"></span>
		</van-col>
		<van-col  span="12" class="pl10 pr10  cr_888">
			<van-icon name="contact" v-text="' '+item.nickname"></van-icon>
		</van-col>
		<van-col  span="12" class="pl10 pr10  cr_888 text-right">
			<van-icon name="password-view " v-text="' '+item.hits"></van-icon>
		</van-col>
		<van-col span="24" class="mt10"><div class="van-hairline--top"></div></van-col>
		<van-col  span="24" class="mt15  f14 p10 content">
			<span v-html="item.content"></span>
		</van-col>
	</van-row>
	
	<van-cell-group class="mt10 mb60">
		<van-cell title="全部评论"></van-cell>
		<van-cell v-for="(v,index) in commont" >
				<van-col span="2"><img :src="v.thumb" class="wd100 brus50"></van-col>
				<van-col span="22" class="pl10">
					<p v-text="v.nickname"></p>
					<p v-html="v.says" class="f12"></p>
				</van-col>
		</van-cell>
	</van-cell-group>
	
	<van-popup v-model="show" position="top" class="unews-d-sub">
		<van-col span="24">
	  		<van-field  v-model="message" type="textarea" placeholder="请输入留言" rows="10"	></van-field>
	  	</van-col>
	  	<van-col span="24" class="mb15 text-center">
	  		<van-button type="default" @click="show=false">关闭</van-button> <van-button type="primary" class="ml15" @click="onSub">评论</van-button>
	  	</van-col>
	</van-popup>
	<div class="mt60">&nbsp;</div>
	<van-row class="van-contact-list-bottom bg_ff text-center pt10 pb10">
	 	  <van-col span="16">
		    <p  class="bg_hs4 pt5 pb5 cr_hs1 unews-d-c"  @click="show=true">说点什么吧...</p>
		  </van-col>
		  <van-col span="4" >
			    <van-icon name="pending-evaluate" :info="cmt" class="icon-bottom f20 mt5"  @click="onCmtbtn"></van-icon>
		  </van-col>
		  <van-col span="4">
			    <van-icon name="like-o" :info="zan" class="icon-bottom f20 mt5" @click="onZan" ></van-icon>
		  </van-col>
	</van-row>
	
</div>
<?php echo template('mobile/script');?>
<?php echo template('mobile/share');?>
<script src="<?php echo JS_PATH.'jquery.min.js'?>"></script>
<script src="<?php echo JS_PATH.'mobile/lazyload.min.js'?>"></script>
<script>
new Vue({
	el: '#app',
	data: {
		item: <?php echo json_encode($item);?>,
	    show:false,
	    message:"",
	    commont:'',zan:0,cmt:0
	},
  	methods: {
  		onSub(){
  	  		if(!this.message){
  	  	  		this.$toast('留言不能为空');
  	  	  		return ;
  	  	  	}
  			axios.post('/api/unews/cmt', Qs.stringify({uid:<?php echo $U['id']?>,id:<?php echo $item['id']?>,say:this.message}),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	      	if(data.state==1){
  		  	   		var s = {"thumb":"<?php echo $U['thumb']?>","nickname":"<?php echo $U['nickname']?>","says":this.message};
  	  		  	   	this.commont.push(s);
  	  		  	   	this.show = false;
  	  	  		  	this.$toast(data.message);
  	  	  	  	}else{
  					this.$toast(data.message);
  	  	  	  	}
  	  	    });	
  	    },
  	    onCmtbtn(){
 	    	 document.documentElement.scrollTop = document.documentElement.clientHeight + 60
  	  	},
  	  	onZan(){
  	  	  	axios.post('/api/unews/cmt_zan', Qs.stringify({uid:<?php echo $U['id']?>,id:<?php echo $item['id']?>}),ajaxconfig).then((response)=> {
	  	  	  	var data = response.data;
	  	      	if(data.state==1){
		  	      	this.zan += 1;
	  	  	  	}else{
					this.$toast(data.message);
	  	  	  	}
	  	    });	
  	  	}
  	},
  	mounted:function (){
  		axios.post('/api/unews/cmt_list', Qs.stringify({id:<?php echo $item['id']?>}),ajaxconfig).then((response)=> {
  	  	  	var data = response.data;
  	      	if(data.state==1){
	  	   		this.commont = data.data.items;
		  	   	this.zan = data.data.zan;
			  	this.cmt = data.data.cmt;
  	  	  	}
  	    });
	}
});
$("img").each(function () {
	var oImgSrc = $(this).attr("src");
	$(this).attr("src", "").attr("data-original", function () {
		return oImgSrc;
	}); 
});
$("img").lazyload({effect: "fadeIn"});
</script>
</body>
</html>