<div class="mt60">&nbsp;</div>
<van-tabbar v-model="active">
	  <van-tabbar-item icon="wap-home" url="/" ><span v-text="'首页'"></span></van-tabbar-item>
	  <van-tabbar-item icon="chat" url="/mobile/group/index">
		  <template slot="icon" slot-scope="props">
	      	<i class="iconfont icon-fenlei" ></i>
	      </template>
	  	    <span v-text="'分类'"></span>
	  </van-tabbar-item>
	  <van-tabbar-item url="/mobile/unews/index">
	  	  <template slot="icon" slot-scope="props">
	      	<i class="iconfont icon-iconset0394" ></i>
	      </template>
	      <span v-text="'游记'"></span>
	  </van-tabbar-item>
	  <van-tabbar-item url="/mobile/pt/index">
	 	  <template slot="icon" slot-scope="props">
	      	<i class="iconfont icon-zuji1" ></i>
	      </template> 
	      <span v-text="'自由行'"></span>
	 </van-tabbar-item>
	  <van-tabbar-item  url="/mobile/user/index">
	  	  <template slot="icon" slot-scope="props">
	      	<i class="iconfont icon-huiyuan" ></i>
	      </template>
	      <span v-text="'我的'"></span>
	  </van-tabbar-item>
</van-tabbar>