<?php echo template('admin/header');template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<div class="row">
			<div class="sysNotice col">
				<blockquote class="layui-elem-quote title">订单详情</blockquote>
				<table class="layui-table">
					<colgroup><col width="150"><col><col width="150"><col></colgroup>
					<tbody>
						<tr>
							<td>订单编号</td><td ><?php echo $item['oid'];?></td>
							<td>订单状态</td>
							<td >
								<?php 
									if($item['state']==2){echo "<span class='btn btn-xs btn-info'>已报名</span> ";
									}elseif($item['state']==3){echo "<span class='btn btn-xs btn-success'>已结束</span> ";
									}elseif($item['state']==4){echo "<span class='btn btn-xs btn-warning'>已退出</span> ";
									}elseif($item['state']==5){echo "<span class='btn btn-xs btn-danger'>申请退出</span> ";
									}elseif($item['state']==1){echo "<span class='btn btn-xs btn-primary'>未支付</span>";}
								?>
							</td>
						</tr>
						<tr>
							<td>活动时间</td><td ><?php echo $item['stime'];?> ~ <?php echo $item['etime'];?></td>
							<td>活动名字</td><td ><?php echo $item['title'];?></td>
						</tr>
						<tr>
							<td>订单单价</td><td ><?php echo $item['money'];?></td>
							<td>订单总价</td><td ><?php echo $item['total'];?></td>
						</tr>
						<tr>
							<td>下单时间</td><td ><?php echo format_time($item['addtime']);?></td>
							<td>支付时间</td><td ><?php echo format_time($item['paytime']);?></td>
						</tr>
						<tr>
							<td>消耗积分</td><td ><?php echo $item['integral'];?></td>
		              		<td>微信单号</td><td><?php echo $item['wx_oid'];?></td>
						</tr>
					</tbody>
				</table>
				<?php if($item['tv']){ $tv = json_decode($item['tv'],true);?>
				<blockquote class="layui-elem-quote title">集合地</blockquote>
				<table class="layui-table">
					<colgroup><col width="150"><col></colgroup>
					<tbody>
						<tr>
							<td>详情</td><td><?php echo $tv['tvname'];?> , <?php echo $tv['tvusername'];?> , <?php echo $tv['tvmobile'];?> , <?php echo $tv['tvaddress'];?> , <?php echo $tv['tvtime'];?></td>
						</tr>
					</tbody>
				</table>
				<?php } ?>
				<?php if($item['info']){ $info = json_decode($item['info'],true);?>
				<blockquote class="layui-elem-quote title">报名联系人</blockquote>
				<table class="layui-table">
					<colgroup><col width="150"><col></colgroup>
					<tbody>
						<?php $i=1;foreach ($info as $v){?>
						<tr>
							<td>联系人<?php echo $i;?></td><td><?php echo $v['username'];?> , <?php echo $v['mobile'];?> , <?php echo $v['code'];?></td>
						</tr>
						<?php $i++;}?>
					</tbody>
				</table>
				<?php } ?>
				<?php if($item['tgs_goods']){ $tgs_goods = json_decode($item['tgs_goods'],true);?>
				<blockquote class="layui-elem-quote title">配件商品</blockquote>
				<table class="layui-table">
					<colgroup><col width="150"><col></colgroup>
					<tbody>
						<?php $i=1;foreach ($tgs_goods as $v){?>
						<tr>
							<td>商品<?php echo $i;?></td><td><?php echo $v['tgsname'];?> , x <?php echo $v['num'];?> , <?php echo $v['tgsmoney'];?></td>
						</tr>
						<?php $i++;}?>
					</tbody>
				</table>
				<?php } ?>
			</div>
		</div>
		
		<?php if($item['state']==5){?>
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">申请退款 (交易时间超过一年的订单无法提交退款；)</div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">退款金额</label>
				<div class="layui-input-block">
					<input type="text" name="data[total]" class="layui-input" value="<?php echo $item['total'];?>" cname="退款金额" lay-verify="required" >
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="hidden" name="data[uid]" value="<?php echo $item['uid']?>">
					<input type="hidden" name="data[id]" value="<?php echo $item['id']?>">
					<input type="hidden" name="data[wx_oid]" value="<?php echo $item['wx_oid']?>">
					<?php echo admin_btn('/adminct/tourismorder/back','save','layui-btn-lg',"lay-filter='sub' location='$index_url' tips='ok'")?>
				</div>
			</div>
		</form>
		<?php }?>
		
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>