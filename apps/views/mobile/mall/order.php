<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">

	<van-nav-bar title="订单安全支付"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	
	<mall-contact-card :type="cardType" :name="currentContact.name" :address="currentContact.address" :tel="currentContact.tel" @click="showList = true"></mall-contact-card>
	
	<van-popup v-model="showList" position="bottom" style="height:100%;">
	  	<van-address-list v-model="chosenContactId"  :list="list"   @add="onAdd"  @edit="onEdit"  @select="onSelect"  ></van-address-list>
	</van-popup>
	
	<van-popup v-model="showEdit" position="bottom">
		<van-address-edit :address-info="addressInfo"  :area-list="areaList" show-postal :show-delete="isEdit" show-set-default  @save="onSave"  @delete="onDelete"></van-address-edit>
	</van-popup>
	<van-card class="bg_ff" v-for="(v,index) in goods" :title="v.name"  :desc="v.options.options"  :num="v.qty"  :price="v.price"  :thumb="v.options.thumb"></van-card>
	
	<van-radio-group v-model="send">
		<van-cell-group class="mt10 order-pay">
			<van-cell ><van-tag mark type="danger" v-text="'配送方式'"></van-tag></van-cell>
			<van-cell><van-radio  name="1" >快递 包邮</van-radio></van-cell>
			<van-cell><van-radio  name="2" >上门 自提</van-radio></van-cell>
		</van-cell-group>
	</van-radio-group>
	
	<van-cell-group class="mt10">
		<van-cell ><van-tag mark type="danger" v-text="'买家留言'"></van-tag></van-cell>
	  	<van-cell >
	  		<van-field v-model="message" type="textarea" placeholder="请输入留言"  clearable rows="1"  autosize></van-field>
	  	</van-cell>
	</van-cell-group>
	
	<van-radio-group v-model="payRadio">
		<van-cell-group class="mt10 order-pay">
			<van-cell ><van-tag mark type="danger" v-text="'支付方式'"></van-tag></van-cell>
			<van-cell><van-radio name="1" >微信支付</van-radio></van-cell>
		</van-cell-group>
	</van-radio-group>
	
	<van-submit-bar  :price="total" class="bg-main" button-text="支付" button-type="primary"  :disabled="chosenContactId?false:true" @submit="onSubmit"></van-submit-bar>
</div>
<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/contact.js'?>" ></script>
<script type="text/javascript" src="<?php echo JS_PATH.'area.js'?>" ></script>
<script>
var uid = <?php echo $U['id']?>;
new Vue({
	el: '#app',
	data: {
		chosenContactId: '<?php echo $aid; ?>',
	    editingContact: {},
	    showList: false,
	    showEdit: false,
	    isEdit:false,
	    list: <?php echo $address; ?>,
	    areaList:areaList,
	    addressInfo:[],
	    goods:<?php echo $items; ?>,
	    payRadio: '1',
	    send:'1',message:'',total:<?php echo $total?>
	},
	computed: {
		cardType() {
		      return this.chosenContactId !== null ? 'edit' : 'add';
		},
	    currentContact() {
	        const id = this.chosenContactId;
	        return id !== null ? this.list.filter(item => item.id === id)[0] : {}
	    }
	},
  	methods: {
  		onSubmit(){
  			var data = [];
  	  		data['aid'] = this.chosenContactId;
  	  	  	data['message'] = this.message?this.message:0;
  	  	  	data['delive'] = this.send;
  	  	  	var l = this.$toast.loading({duration: 0,mask: true,message: '支付中...'});
  			axios.post('/mobile/mall/create_order', Qs.stringify(data),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
  	  	  	      	callpay(data);
  	  	  	  	}else{
					this.$toast(data.message);
  	  	  	  	}
  	  	    });
  	  	},
  	    onAdd() {
  	    	this.isEdit = false;
  	      	this.showEdit = true;
    	    this.addressInfo = '';
  	    },
  	    onEdit(item) {
  	  	  	this.isEdit = true;
  	      	this.showEdit = true;
  	      	this.addressInfo = item;
  	    },
  	    onSelect() {
  	      this.showList = false;
  	    },
  	    onSave(info) {
    	    info.address = info.province+info.city+info.county+info.address_detail;
    	    info.uid = uid;
    	    var url = this.isEdit?'/api/address/edit':'/api/address/insert';
    	    var l = this.$toast.loading({duration: 0,mask: true,message: '保存中...'});
  	    	axios.post(url, Qs.stringify({info}),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
  	  	  	      	this.showList = false;
	      	      	if (this.isEdit) {
	      	        	this.list = this.list.map(item => item.id === info.id ? info : item);
	      	      	} else {
	      	        	this.list.push(info);
	      	      	}
	      	      	this.chosenContactId = info.id;
  	  	  	  	}
  	  	  	  	this.$toast(data.message);
  	  	  	  	this.showEdit = false;
  	  	    });
  	    },
  	    onDelete(info) {
  	    	var l = this.$toast.loading({duration: 0,mask: true,message: '删除中...'});
  	    	axios.post('/api/address/del', Qs.stringify({uid:uid,id:info.id}),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
  	  	  	      	this.showEdit = false;
  	  	  	      	this.list = this.list.filter(item => item.id !== info.id);
  	  	  	      	if (this.chosenContactId === info.id) {
  	  	  	        	this.chosenContactId = null;
  	  	  	      	}
  	  	  	  	}
  	  	    });
  	    },
  	    get_content(){
  	    	var l = this.$toast.loading({duration: 0,mask: true,message: '加载联系人'});
  	    	axios.post('/api/address/lists', Qs.stringify({uid:uid,address:<?php echo $U['address']?>}),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
    	  	   		this.list = data.data;
  	  	  	  	}
  	  	    });
  	  	}
  	},
  	mounted:function(){
  		this.get_content();
  	}
});
</script>
<?php echo template('mobile/pay');?>
</body>
</html>