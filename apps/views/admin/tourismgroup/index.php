<?php echo template('admin/header');echo template('admin/sider');?>
<!-- 修改为首页导航 -->
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline ">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<table  id="user" lay-filter="common" ></table>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/edit/id-{{d.id}}'),'edit','layui-btn-xs');?>
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="sth">
<input type="checkbox" lay-text='显示|关闭' lay-skin="switch" lay-filter='open' {{# if(d.state==1){ }} checked {{#  } }}   data-url="<?php echo site_url($dr_url.'/lock/id-{{d.id}}')?>" >
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full-250', //容器高度
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'sort', title: '排序', width: 80,edit:'text'},
	       {field: 'tgname', title: '名称',edit:'text'},
	       {field:'thumb',title:'图标',toolbar:'<div><div class="img_view"><img src="{{d.icon}}"></div></div>'},
	       {field: 'state', title: '状态',toolbar: '#sth',width: 90},
	       {field: 'right', title: '操作',toolbar:'#operation', width: 220}
	       ]],
	limit:15,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});
layui.table.on('edit(common)', function(obj){
	var data = {id:obj.data.id},key = "data["+obj.field+"]";
	data[key] = obj.value;
	$.post('<?php echo site_url("$dr_url/edits")?>',data,function(d){layer.msg(d.message)},'json');
});
</script>
<?php echo template('admin/footer');?>