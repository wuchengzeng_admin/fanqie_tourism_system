<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入昵称" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
				<div class="layui-inline">
					<?php echo admin_btn(site_url("$dr_url/dels"),'dels','layui-btn-danger f_dels');?>
				</div>
		</blockquote>
		<table  id="user" lay-filter="common" ></table>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/html" id="operation">
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
<?php echo admin_btn(site_url($dr_url.'/set/id-{{d.id}}'),'del','layui-btn-xs layui-bg-green f_del_d','lay-event="del"','封面');?>
</script>

<script>
//执行渲染
var tab = layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full-250', //容器高度
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {checkbox: true},
	       {type:'numbers',title: 'No.'},
	       {field: 'id', title: 'ID', width: 80,sort:true},
	       {field: 'nickname', title: '昵称'},
	       {field:'thumb',title:'图片',toolbar:'<div><div class="img_view"><img src="{{d.thumb}}"></div></div>'},
	       {field: 'pname', title: '分组名称'},
	       {field: 'addtime', title: '添加时间',toolbar:'<div>{{Time(d.addtime, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'right', title: '操作',toolbar: '#operation',width: 150}
	       ]],
	limit: 15,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});
</script>
<?php echo template('admin/footer');?>