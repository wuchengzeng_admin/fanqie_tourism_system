<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote">
			<div class="layui-inline">
					<?php echo admin_btn(site_url($add_url.'/tid-'.$tid),'add','layui-btn-normal');?>
			</div>
			<span class="thumb-say">（温馨提示：非特殊情况下请不要修改以下数据，否则会影响订单！）</span>
		</blockquote>
		<form class="layui-form" method="post">
		  	<table class="layui-table">
			    <thead>
					<tr>
						<th>ID</th>
						<th>出发时间</th>
						<th>成行人数</th>
						<th>总人数</th>
						<th>报名人数</th>
						<th>操作</th>
					</tr> 
			    </thead>
			    <tbody>
                    <?php if (empty($items)){?>
                    <tr>
						<td class="empty-table-td"><?php echo $emptyRecord;?></td>
					</tr>
                    <?php }?>
					<?php foreach ($items as $k=>$v){ ?>
                     <tr id="list_<?php echo $v['id']?>">
						<td>
                           <?php echo $v['id']; ?>
                           <input type="hidden" name="hids[<?php echo $v['id']; ?>]" value="<?php echo $v['id']; ?>" />
						</td>
						<td><input type="text" name="data[<?php echo $v['id']; ?>][go_time]" value="<?php echo $v['go_time']; ?>" class="layui-input time" lay-verify="required" /></td>
						<td><input type="text" name="data[<?php echo $v['id']; ?>][inline]" value="<?php echo $v['inline']; ?>" class="layui-input" lay-verify="required" /></td>
						<td><input type="text" name="data[<?php echo $v['id']; ?>][full]" value="<?php echo $v['full']; ?>" class="layui-input" lay-verify="required" /></td>
						<td><?php echo $v['sign_num']; ?></td>
						<td>
						  <?php echo admin_btn(site_url($dr_url.'/order/ttid-'.$v['id'].'-tid-'.$tid),'','layui-btn-xs','','订单');?>
						  <?php echo admin_btn(site_url($dr_url.'/del/id-'.$v['id']),'del','layui-btn-xs f_del');?>
						</td>
					</tr>
                    <?php }?>
	             </tbody>
			</table>
			<footer class="panel-footer">
				<div class="layui-row">
				    <div class="layui-col-xs6">
				    	<div class="layui-inline">
							<?php echo admin_btn(site_url($dr_url.'/quicksave'), 'save','',"lay-filter='sub' location=''");?>
						</div>
				    </div>
				</div>
			</footer>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script>
$('.time').each(function(){
	layui.laydate.render({ 
		  elem: this,
		  range: '~'
	});	
});
</script>
<?php echo template('admin/footer');?>