<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 用户文章管理
 * @author chaituan@126.com
 */
class Unews extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/UserNews_model'=>'do'));
	}
	
	public function index() {
		$this->load->view ('admin/unews/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"title like '%$name%'":'';
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$data['addtime'] = time();
			is_AjaxResult ( $this->do->add ( $data ) );
		} else {
			$data ['cat'] = $this->do->get_cache();
			$this->load->view ( 'admin/unews/add', $data );
		}
	}
	
	public function detail() {
		$id = Gets("id", "checkid");
		$data['item'] = $this->do->getItem("id=$id");
		$this->load->view('admin/unews/detail', $data);
	}
	
	
	function del() {
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
	public function dels() {
		$data = Posts();
		if (!$data)AjaxResult_error('没有选中要删除的');
		$ids = implode(',', $data['checked']);
		$result = $this->do->deletes("id in ($ids)");
		if ($result) {
			AjaxResult(1,"删除成功",$data['checked']);
		} else {
			AjaxResult(2,"删除失败");
		}
	}
}
