<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-swipe :autoplay="3000" class="bg_ff">
	  <van-swipe-item v-for="(v, index) in aditems" :key="index" class="text-center">
	  	<a :href="v.url"><img v-lazy="v.thumb" ></a>
	  </van-swipe-item>
	</van-swipe>
	<template v-if="userorder">
	<div class="ct-flexbox bg_ff p5">
		<img src="/res/images/mobile/kb.png" class="wd10 mr5">
	    <yd-rollnotice autoplay="2000">
	        <yd-rollnotice-item v-for="(v,index) in userorder" >
	        	<a :href="'/mobile/tourism/detail/id-'+v.tid" class="ellipsis">
	        		<span v-text="v.nickname+' 发起了'"></span> <span style="color:#F00;" v-text="v.title"></span><span v-text="' 活动，邀请你一起去旅行'"></span> 
	        	</a>
	        </yd-rollnotice-item>
	    </yd-rollnotice>
	</div>
	</template>
	
	<van-tabs class="mt10 bg_ff" @click="getList">
	  <van-tab v-for="(v,index) in tree" :title="v.tname" >
	  		<template v-if="ptitems">
				<van-row class="pl5 pr5 mt10 pt-money-main" v-for="(v,index) in ptitems" :key="v.id">
					<a :href="'/mobile/pt/detail/id-'+v.id" >
						<van-col  span="24" class="text-center"><img v-lazy="v.thumb" ></van-col>
						<van-col span="24"><span class="l-h24 f14 pl5" v-text="v.title"></span></van-col>
						<van-col span="24" class="pl5">
							<span class="cr_888" v-text="v.day_num+'天'"></span> 
							<span class="cr_888" v-text="'难度：'"></span>
							<i class="iconfont icon-xingxing f12 cr_888" v-for="d in parseInt(v.diff)" ></i>
							<span class="cr_888" v-text="v.diff_say"></span>
						</van-col>
						<span class="pt-money bg-full cr_ff pl5 pr5" v-text="v.money+'元/人起'"></span>
					</a>
				</van-row>
	  		</template>
			<template v-else><div class="p10 mt10 text-center cr_hs2" v-text="'没有数据'"></div></template>
	  </van-tab>
	</van-tabs>
	<?php echo template('mobile/tabbar');?>
</div>


<?php echo template('mobile/script');?>
<?php echo template('mobile/share');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/rollnotice.js'?>" ></script>
<script>
//滚动公告
Vue.component(ydui.RollNotice.name, ydui.RollNotice);
Vue.component(ydui.RollNoticeItem.name, ydui.RollNoticeItem);
new Vue({
	el: '#app',
	data: {
		active:3,
		aditems: '',
		ptitems:'',
		ptclick:[],
		tree: <?php echo $items;?>,
		userorder:'',
		lid:<?php echo $lid;?>
	},
  	methods: {
  	  	getList(index){
  	  	  	var id = this.tree[index].id;
  	  	  	if(!Array.isArray(this.ptclick[index])){
  	  	  	  	var l = this.$toast.loading({message: '加载中...'});
  	  	  	  	this.ptclick.push(index);
  	  	  	  	axios.post('/api/pt/lists', Qs.stringify({tgid:id,lid:1}),ajaxconfig).then((response)=> {
  		  	  	  	var data = response.data;
  		  	      	if(data.state==1){
  	  		  	  	    this.ptclick[index] = data.data;
  	  	  		  	    this.ptitems = data.data;
  		  	  	  	}else{
  	  		  	  	    this.ptclick[index] = [];
  			  	  	  	this.ptitems = '';
  				  	}
  			  	    l.clear();
  		  	    });
  	  	  	}else{
  	  	  	  	this.ptitems = this.ptclick[index].length?this.ptclick[index]:'';
  	  	    }
  	  	},
  	    load_ad:function(){
			var that = this;
			//加载全部ajax		  	   		
			axios.all([get_ad(),get_userorder()]).then(axios.spread(function (ad,hd) {
				var aditem = ad.data,hditems = hd.data;
				if(aditem.state==1){
		  			that.aditems = aditem.data;
			  	}else{
				  	that.ad_load = false;
			  		that.$toast(aditem.message);
				}
				if(hditems.state==1){
					that.userorder = hditems.data;
				}
			}));
	    }
  	},
  	mounted:function (){
  		this.load_ad();
  		if(this.lid){
  			this.getList(0);
  	  	}
  		
	}
});

function get_ad() {//获取广告
	  return axios.post('/api/home/ad',Qs.stringify({cid:3}),ajaxconfig);
}
function get_userorder() {//获取广告
	  return axios.post('/api/pt/userorder','',ajaxconfig);
}


</script>
</body>
</html>