<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Ad extends WechatCommon {
	
	public function index() {
		$this->load->model(array('admin/Ad_model'));
		$items = $this->Ad_model->getItems(array('catid'=>1,'state'=>1));
		AjaxResult(1,'ok',$items);
	}
}
