<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Travel extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Travel_model'=>'do','admin/TravelPhoto_model'=>'do_group'));
	}
	
	function index() {
		$this->load->view('admin/travel/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"nickname like '%$name%'":'';
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	function del(){
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		$this->do_group->deletes(array('tid'=>$id));
		is_AjaxResult($result);
	}
	
}
