<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 活动标签
 * @author chaituan@126.com
 */
class Tourism_tag extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/TourismTag_model','do');
	}
	
	public function index() {
		$this->load->view('admin/tourism_tag/index');
	}
	
	public function iframe() {
		$data['name'] = $name = Gets('name');//搜索
		$where = $name?"tname like '%$name%'":'';
		$data['items'] = $this->do->getItems($where);
		$this->load->view('admin/tourism_tag/iframe', $data);
	}
	
	function lists(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"tname like '%$name%'":'';
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			if ($this->do->add($data)) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$this->load->view ('admin/tourism_tag/add');
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','num')));
			$this->load->view('admin/tourism_tag/edit',$data);
		}
	}
	
	function sort(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$data = Posts('data');
			$result = $this->do->updates($data,array('id'=>$id));
			is_AjaxResult($result);
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		if($this->do->deletes("id=$id")){
			$this->do->deletes(array('parent_id'=>$id));
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
