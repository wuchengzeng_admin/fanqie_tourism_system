<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class News extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		parseURL($this->uri->segment(4));
		$this->load->vars('web_name',admin_config_cache('wechat')['wechat_name']);
	}
	
	function lists() {
		$this->load->model(array('admin/News_model','admin/NewsCat_model'));
		$cid = Gets('cid');
		$data['catid'] = $cid;
		$result = $this->News_model->getItems($data,'id,thumb,title');
		$data['items'] = $result?json_encode($result):0;
		$data['item'] = $this->NewsCat_model->getItem(array('id'=>$cid),'catname');
		$this->load->view('mobile/news/lists',$data);
	}
	
	function detail(){
		$data['id'] = Gets('id');
		$this->load->model(array('admin/News_model'));
		$data['item'] = $this->News_model->getItem($data,'title,addtime,content');
		$this->load->view('mobile/news/detail',$data);
	}
}
