<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 微信登录
 *
 * @author chaituan@126.com
 */
use EasyWeChat\Foundation\Application;
class Test extends CI_Controller {
	
	// 注册
	public function index() {
		$this->load->model(array('admin/User_model','admin/TourismOrder_model','admin/TourismChild_model','admin/Tourism_model','admin/Integral_model'));
		$item = $this->TourismOrder_model->getItem(array('id'=>14,'state'=>1));
		$num = count(json_decode($item['info'],true));
		if($item['tcid']){
			//更新子活动
			$this->update_child($item['tcid'], $num);
		}else{//子id没有值的时候 需要创建 子活动
			$stime = $item['stime'];$etime = $item['etime'];$tid = $item['tid'];$uid = $item['uid'];
			//是否该日期已经存在
			$is_child = $this->TourismChild_model->getItem(array("stime"=>$stime),'id');
			if($is_child){//当存在的时候 就不再创建新的活动 执行更新
				$tcid = $is_child['id'];
				$this->update_child($tcid, $num);
			}else{
				$tourism = $this->Tourism_model->getItem(array('id'=>$tid),'line_num,in_line,full');
				$data = array('uid'=>$item['uid'],'nickname'=>$item['nickname'],'tid'=>$tid,'stime'=>$stime,'etime'=>$etime,'line_num'=>$tourism['line_num'],'in_line'=>$tourism['in_line'],'full'=>$tourism['full'],'sign_num'=>$num+1,'addtime'=>time());
				$tcid = $this->TourismChild_model->add($data);
			}
			if($tcid)$this->TourismOrder_model->updates(array('tcid'=>$tcid,'stime'=>$stime,'etime'=>$etime),array("id"=>$item['id']));
		}
	}
	
	
	private function update_child($tcid,$num){
		$this->load->model(array('admin/TourismChild_model'=>'do'));
		$child = $this->do->getItem(array('id'=>$tcid),'sign_num,uid,line_num');
		$edit = array('sign_num'=>"+=$num");
		if($child['uid']){//会员发起 要判断是否要上线
			$total_num = $num + $child['sign_num'];
			if($total_num >= $child['line_num']){//到达指定的数量就执行
				$edit['state'] = 1;
			}
		}
		$this->do->updates($edit,array('id'=>$tcid));
	}
	
	function user(){
		$this->load->model(array('admin/User_model'=>'do'));
		$items = $this->do->getItems('','nickname,id');
		foreach ($items as $v){
			$this->do->updates(array('nickname'=>get_Nickname($v['nickname'])),array('id'=>$v['id']));
		}
		exit;
	}
	
	function img(){
		var_dump(get_Cache('chaituan'));
		exit;
	}
}
