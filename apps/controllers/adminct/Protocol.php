<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 广告管理
 * @author chaituan@126.com
 */
class Protocol extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Protocol_model'=>'do'));
	}
	
	public function index() {
		$data['item'] = $this->do->getItem();
		$this->load->view ('admin/protocol/index',$data);
	}
	
	function edit(){
		if(is_ajax_request()){
			$this->do->updates(array('content'=>$_POST['content']),array('id'=>1));
			AjaxResult_ok();
		}
	}
}
