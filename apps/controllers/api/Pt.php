<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 自由行数据
 * @author chaituan@126.com
 */
class Pt extends CI_Controller {
	
	
	function lists(){
		if(is_ajax_request()){
			$tgid = Posts('tgid','num');
			$lid = Posts('lid','num');
			$this->load->model(array('admin/Tourism_model'=>'do'));
			$items = $this->do->getItems(array('tgid'=>$tgid,'is_user'=>1,"lid in ($lid,0)"),'id,thumb,title,money,day_num,diff');
			$diff = get_diff();
			foreach ($items as $v){
				$v['diff_say'] = $diff[$v['diff']];//难度解析
				$news[] = $v;
			}
			
			if($items){
				AjaxResult(1, 'ok',$news);
			}else{
				AjaxResult_error("没有数据");
			}
		}
	}
	
	function userorder(){
		if(is_ajax_request()){
			$time = format_time(time(),'Y-m-d');
			$this->load->model(array('admin/TourismChild_model'=>'do'));
			$items = $this->do->getItems_join(array('tourism'=>"tourism.id=tourism_child.tid+left"),"tourism_child.uid<>0 and (tourism_child.stime>'$time' or tourism_child.etime>'$time')",'tourism.title,tourism_child.id,tourism_child.nickname,tourism_child.tid','tourism_child.id desc');
			if($items){
				AjaxResult(1, '',$items);
			}else{
				AjaxResult_error();
			}
		}
	}
	
	function get_time(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$time = Posts('time');
			$this->load->model(array('admin/TourismChild_model'));
			$item = $this->TourismChild_model->getItem(array('tid'=>$id,'stime'=>$time),'stime,full,sign_num,id,tid');
			if($item){
				AjaxResult(1, 'ok',array('time'=>format_time(strtotime($item['stime']),'m月d号'),'num'=>$item['full'] - $item['sign_num'],'url'=>site_url('mobile/tourism/detail/id-'.$item['tid'].'-cid-'.$item['id'])));
			}else{
				AjaxResult_error('数据异常');
			}
		}
	}
	
	function creat(){
		if(is_ajax_request()){
			$data = Posts('data');
			$day_num = $data['day_num'];$id = $data['id'];$stime = $data['stime'];$etime = '';
			if(!$day_num||!$id||!$stime)AjaxResult_error('数据错误');
			if($day_num > 1){
				$day_num = $day_num - 1;
				$etime = date("Y-m-d",strtotime("$stime +$day_num day"));
			}
			$this->session->child_time = array('stime'=>$stime,'etime'=>$etime);
			AjaxResult(1, '操作成功',site_url('mobile/tourism/order/id-'.$id));
		}
	}
	
}
