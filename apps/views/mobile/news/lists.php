<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="<?php echo $item['catname'];?>"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	
	<template v-if="list">
	<van-cell-group>
	  	<van-cell v-for="(v,index) in list" :title="v.title" is-link :url="'/mobile/news/detail/id-'+v.id+'.html'" ></van-cell>
	</van-cell-group>
	</template>
	<template v-else><p class="cr_hs2 text-center mt60"><i class="iconfont icon-meiyoujieguo f80 d_block"></i><span v-text="'什么也木有~'"></span></p></template>
	
</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		list:<?php echo $items;?>
	}
});
</script>
</body>
</html>