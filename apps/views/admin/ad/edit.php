<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">编辑</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form  a-e-form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">文章分组</label>
					<div class="layui-input-block">
						<select name="data[catid]" >
			            <?php foreach ($cat as $v){?>
			            	<option value="<?php echo $v['id'];?>" <?php if($v['id']==$item['catid']) echo "selected";?>>  <?php echo $v['gname'];?></option>
			            <?php }?>
			        	</select>
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">标题</label>
					<div class="layui-input-block">
						<input type="text" name="data[title]" class="layui-input" lay-verify="required" value="<?php echo $item['title']?>" >
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">链接</label>
					<div class="layui-input-block">
						<input type="text" name="data[url]" class="layui-input" placeholder="不填写为空的时候，默认为 #号，请不要删除" value="<?php echo $item['url']?>" lay-verify="required">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">图片</label>
					<div class="layui-input-block">
						<?php echo admin_btn('/images/upload.html', 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最宽640px</span>
						<div class="layui-upload layui-hide">
						  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
						  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
					  	</div>
					  	<input name="data[thumb]" class="thumb" type="hidden" lay-verify="thumb|required" value="<?php echo $item['thumb']?>" />
					</div>
				</div>
				
				<div class="layui-form-item">
					<div class="layui-input-block">
						<input type="hidden" name="id" value="<?php echo $item['id']?>"> 
						<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
					</div>
				</div>
	</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
});
</script>
<?php echo template('admin/footer');?>