<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header'); ?>
<div id="app">
	<van-tabs :active="active" sticky @click="getList"  class="my-order">
		<van-tab v-for="(v,i) in tree" :title="v" class="td_content" >
			<template v-if="list">
				<van-cell-group class="mt10" v-for="(v,index) in list">
				  <van-cell title="订单号" icon="pending-orders" :value="v.oid" is-link :url="'/mobile/tourism/my_odetail/id-'+v.id"></van-cell>
				  <van-cell :url="'/mobile/tourism/my_odetail/id-'+v.id">
					  <van-col span="4"><img v-lazy="v.g_thumb"></van-col>
					  <van-col span="15" class="pl10"> <p class="l-h1" v-text="v.title"></p><p class="cr_hs2 f12" v-text="'出发：'+v.stime"></p></van-col>
					  <van-col span="5" class="text-right"><p class="l-h1" v-text="'￥'+v.total"></p><p class="cr_hs2 f12" v-text="'x '+JSON.parse(v.info).length"></p></van-col>
				  </van-cell>
				  <van-cell >
			  		  <div class="van-cell__value">
			  			<van-button size="small" v-if="v.state==2">退出</van-button>
			  			<van-button size="small" v-if="v.state==2" type="danger">确认参与</van-button>
			  			<!-- <van-button size="small" v-if="v.state==3||v.state==4">删除</van-button> -->
			  			<van-button size="small" v-if="v.state==3">评价</van-button>
			  			<van-button size="small" v-if="v.state==3" type="danger" tag="a" :href="'/mobile/photo/upload/id-'+v.tid">发布相册</van-button>
			  			<van-button size="small" v-if="v.state==5" >退款申请中</van-button>
			  		  </div>
				  </van-cell>
				</van-cell-group>
			</template>
			<template v-else><p class="cr_hs2 text-center mt60"><i class="iconfont icon-meiyoujieguo f80 d_block"></i><span v-text="'什么也木有~'"></span></p></template>
		</van-tab>
	</van-tabs>
	<div class="mt60">&nbsp;</div>
</div>


<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		active:<?php echo $id;?>,
		list:"",
		ptclick:[],
		tree:['全部活动','已报名','已结束','已退出']
	},
  	methods: {
  		getList(id){
  			if(!Array.isArray(this.ptclick[id])){
  				var l = this.$toast.loading({message: '加载中...'});
  	  	  	  	this.ptclick.push(id);
	  			axios.post('/mobile/tourism/my_order', Qs.stringify({id:id}),ajaxconfig).then((response)=> {
	  	  	  	  	var data = response.data;
	  	  	      	if(data.state==1){
		  	  	      	this.ptclick[id] = data.data;
	    	  	   		this.list = data.data;
	  	  	  	  	}else{
	  	  	  	  	  	this.ptclick[id] = [];
	  	  	  	  		this.list = "";
	  	  	  	  	}
		  	  	    l.clear();
	  	  	    });
  			}else{
  				this.list = this.ptclick[id].length?this.ptclick[id]:'';
  	  	    }
  	  	}
  	},
  	mounted:function (){
  		this.getList(this.active);
	}
});


</script>
</body>
</html>