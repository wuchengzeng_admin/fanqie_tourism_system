<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 普通图片上传
 * @author chaituan@126.com
 */
class Upload extends CI_Controller {
	function index() {
		header("Content-Type:text/html;charset=utf-8");
		error_reporting (E_ERROR | E_WARNING);
		date_default_timezone_set("Asia/chongqing");
		//上传配置
		
		$path = "/res/upload/images/{yyyy}{mm}{dd}/{time}{rand:6}";
		$config = array ("pathFormat" => $path,"maxSize" => 10000, "allowFiles" => array(".ogg",'.mp4','.webm','.mp3','.wav'));
		$this->load->library('Uploader', array ('fileField' =>'file','config' => $config));
		$info = $this->uploader->getFileInfo();
		if ($info ['state'] == 'SUCCESS') {
			$this->load->model ('Image_model');
			$this->Image_model->add(array('userid' => 0,'thumb' => $info['url'],'filesize' => $info['size'],'type' => $info['type'],'addtime' => time()));
			AjaxResult ( 1, '上传成功', $info );
		} else {
			AjaxResult_error($info['state']);
		}
		
	}
	
	function del() {
		if(is_ajax_request()){
			$thumb = Posts('thumb');
			$thumbs = substr($thumb,1);
			if (is_file($thumbs)) {
				if (unlink( $thumbs )) {
					$this->load->model('Image_model');
					$r = $this->Image_model->deletes(array('thumb' => $thumb));
					is_AjaxResult($r);
				} else {
					AjaxResult_error('删除失败');
				}
			} else {
				AjaxResult_error('操作失败,文件路径出问题');
			}
		}
	}
}
	
	
