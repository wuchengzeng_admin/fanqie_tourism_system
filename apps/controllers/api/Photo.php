<?php
use phpDocumentor\Reflection\Types\This;
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 相册
 * @author chaituan@126.com
 */
class Photo extends CI_Controller {
	
	function get_lists(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$this->load->model(array('admin/Photo_model'));
			$items = $this->Photo_model->getItems(array('tid'=>$id));
			$news = "";
			foreach ($items as $v){
				$img = getimagesize(substr($v['thumb'],1));
				$v['w'] = $img[0];
				$v['h'] = $img[1];
				$news[] = $v;
			}
			if($news){
				AjaxResult(1, '',$news);
			}else{
				AjaxResult_error('没有数据');
			}
		}
	}
}
