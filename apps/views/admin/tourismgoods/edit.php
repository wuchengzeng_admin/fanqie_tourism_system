<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">编辑 </div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">名称</label>
				<div class="layui-input-block">
					<input type="text" name="data[tgsname]" class="layui-input" value="<?php echo $item['tgsname'];?>" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">价格</label>
				<div class="layui-input-block">
					<input type="text" name="data[tgsmoney]" class="layui-input" value="<?php echo $item['tgsmoney'];?>" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">文章图片</label>
				<div class="layui-input-block">
					<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px</span>
					<div class="layui-upload layui-hide">
					  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
					  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
				  	</div>
				  	<input name="data[tgsthumb]" class="thumb" type="hidden" value="<?php echo $item['tgsthumb'];?>" lay-verify="thumb"/>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
				<input type="hidden" name="id" value="<?php echo $item['id']?>">
				<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
});
</script>
<?php echo template('admin/footer');?>