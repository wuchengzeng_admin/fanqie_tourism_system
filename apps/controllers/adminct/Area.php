<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 区域
 * @author chaituan@126.com
 */
class Area extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/Area_model','do');
	}
	
	public function index() {
		$data['items'] = $this->do->getItems();
		$this->load->view('admin/area/index', $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			if ($this->do->add($data)) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$this->load->view ('admin/area/add');
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','num')));
			$this->load->view('admin/area/edit',$data);
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		$this->load->model('admin/Tourism_model');
		$item = $this->Tourism_model->getItem(array('aid'=>$id));
		if($item)AjaxResult_error("删除失败，该区域已和活动关联");
		if($this->do->deletes("id=$id")){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
