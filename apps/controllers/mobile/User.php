<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class User extends WechatCommon {
	function __construct(){
		parent::__construct();
		$this->load->vars('actvie',4);
	}
	function index() {
		$this->load->model('admin/User_model','user');
		$u = $this->user->getItem(array('id'=>$this->User['id']),'gid,fx');
		if($u['gid']==2&&$u['fx']==3){
			$data['gid'] = 3;
		}elseif($u['gid']==2){
			$data['gid'] = 2;
		}else{
			$data['gid'] = 1;
		}
		$this->load->view('mobile/user/index',$data);
	}
	
	function edit(){
		if(is_ajax_request()){
			$data = Posts('data');
			$this->load->model("admin/User_model",'do_user');
			$result = $this->do_user->updates_se(array('nickname'=>$data['nickname'],'username'=>$data['username'],'mobile'=>$data['mobile'],'w_id'=>$data['w_id']),array('id'=>$this->User['id']));
			$this->do_user->up_user_session($data);
			is_AjaxResult($result);
		}else{
			$this->load->view('mobile/user/edit');
		}
	}
	
	
	
	function business(){
		if(is_ajax_request()){
			$data = Posts('data');
			$data['addtime'] = time();
			$data['uid'] = $this->User['id'];
			$this->load->model('admin/Business_model');
			$result = $this->Business_model->add($data);
			is_AjaxResult($result);
		}else{
			$this->load->model('admin/News_model');
			$data['item'] = $this->News_model->getItem(array('id'=>25,'state'=>1),'content');
			$this->load->view('mobile/user/business',$data);
		}
	}
	
	function leader(){
		$this->load->model(array('admin/User_model','admin/Leader_model'));
		if(is_ajax_request()){
			$uid = $this->User['id'];
			$data = Posts('data');
// 			$thumb = array_filter(Posts('thumb'));
// 			if(!$thumb)AjaxResult_error('请上传照片');
// 			if(count($thumb)==3)AjaxResult_error('请上传3张照片');
// 			$data['thumb'] = implode(',', $thumb);
			$data['uid'] = $uid;
			$this->User_model->updates_se(array('username'=>$data['username'],'leader'=>1),array('id'=>$uid));
			$result = $this->Leader_model->add($data);
			is_AjaxResult($result);
		}else{
			$this->load->model('admin/News_model');
			$data['item'] = $this->News_model->getItem(array('id'=>24,'state'=>1),'content');
			$data['user'] = $this->User_model->getItem(array('id'=>$this->User['id']));
			$this->load->view('mobile/user/leader',$data);
		}
	}
	
	function leader_img_add(){
		if(is_ajax_request()){
			$path = "/res/upload/images/{yyyy}{mm}{dd}/{time}{rand:6}";
			$config = array ("pathFormat" => $path,"maxSize" => 1000, "allowFiles" => array(".gif",".png",".jpg",".jpeg"),'oriName'=>'123.jpg');
			$this->load->library('Uploader', array ('fileField' =>'thumb','config' => $config,'upload'=>'base64'));
			$info = $this->uploader->getFileInfo();
			if ($info ['state'] == 'SUCCESS') {
				AjaxResult(1, '添加成功',array('thumb'=>$info['url'],'url'=>site_url('mobile/user/leader_img_del')));
			} else {
				AjaxResult_error($info['state']);
			}
		}
	}
	
	function leader_img_del(){
		if(is_ajax_request()){
			$thumbs = substr(Posts('thumb'),1);
			if (is_file($thumbs)) {
				if (unlink($thumbs)) {
					AjaxResult_ok('删除成功');
				} else {
					AjaxResult_error('删除失败');
				}
			} else {
				AjaxResult_error('操作失败,文件路径出问题');
			}
		}
	}
	
	function integral(){
		$this->load->model('admin/News_model');
		$data['item'] = $this->News_model->getItem(array('id'=>23,'state'=>1),'content');
		$this->load->view('mobile/user/integral',$data);
	}
	
	function integral_list(){
		$this->load->model('admin/Integral_model');
		$result = $this->Integral_model->getItems(array('uid'=>$this->User['id']));
		$data['items'] = $result?json_encode(result_format_time($result)):0;
		$this->load->view('mobile/user/integral_list',$data);
	}
	
	function fx_apply(){
		$this->load->model('admin/User_model');
		if(is_ajax_request()){
			$result = $this->User_model->updates_se(array('fx'=>1),array('id'=>$this->User['id']));
			is_AjaxResult($result,'审核中...');
		}else{
			$this->load->model('admin/News_model');
			$data['item'] = $this->News_model->getItem(array('id'=>27,'state'=>1),'content');
			$data['user'] = $this->User_model->getItem(array('id'=>$this->User['id']));
			$this->load->view('mobile/user/fx_apply',$data);
		}
	}
	
	function city(){
		if(is_ajax_request()){
			$p = Posts('p');
			$c = Posts('c');
			$d = Posts('d');
			$this->load->model(array('admin/User_model'));
			$result = $this->User_model->updates_se(array('province'=>$p,'city'=>$c,'district'=>$d),array('id'=>$this->User['id']));
			is_AjaxResult($result);
		}
		
	}
	
}
