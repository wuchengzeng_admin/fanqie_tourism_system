<?php
use phpDocumentor\Reflection\Types\This;
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Photo extends WechatCommon {
	
	function index() {
		$this->load->model(array('admin/Tourism_model'));
		$table = array('(SELECT thumb,tid,addtime FROM ct_photo a WHERE 3 > (SELECT COUNT(*) FROM ct_photo WHERE tid=a.tid and id>a.id ) ORDER BY a.id DESC) as b'=>"b.tid=tourism.id+left");
		$photo = $this->Tourism_model->getItems_join($table,'','b.thumb,tourism.id,tourism.title,b.addtime');
		$news = '';
		if($photo){
			foreach ($photo as $v){
				$v['addtime'] = $v['addtime']?format_time($v['addtime']):'（等待上传）';
				$news[$v['id']]['item'] = $v;
				if($v['thumb']){
					$news[$v['id']]['thumb_arr'][] = $v['thumb'];
				}else{
					$news[$v['id']]['thumb_arr'] = '';
				}
					
			}	
		}
		$data['items'] = $news;
		$this->load->view('mobile/photo/index',$data);
	}
	
	function lists(){
		$data['id'] = $tid = Gets('id','num');
		$this->load->model(array('admin/Tourism_model'));
		$data['item'] = $this->Tourism_model->getItem(array('id'=>$tid),'thumb,title');
		$this->load->view('mobile/photo/lists',$data);
	}
	
	function upload(){
		$id = $data['id'] = Gets('id','num');
		$this->load->model(array('admin/TourismOrder_model'));
		$item = $this->TourismOrder_model->getItem(array('tid'=>$id,'uid'=>$this->User['id']),'id');
		if(!$item)showmessage('抱歉，你没有权限上传','error');
		$this->load->view('mobile/photo/upload',$data);
	}
	
	function uploads(){
		$id = Posts("id",'num');
		$user = $this->User;
		$path = "/res/upload/images/{yyyy}{mm}{dd}/{time}{rand:6}";
		$config = array ("pathFormat" => $path,"maxSize" => 10000, "allowFiles" => array(".gif",".png",".jpg",".jpeg"));
		$this->load->library('Uploaders', array ('fileField' =>'file','config' => $config));
		$info = $this->uploaders->getFileInfo();
		if(!$info)AjaxResult_error('上传失败');
		foreach ($info as $v){
			if($v['state'] == 'SUCCESS'){
				$data[] = array('tid'=>$id,'thumb'=>$v['url'],'nickname'=>$user['nickname'],'uid'=>$user['id'],'header'=>$user['thumb'],'addtime'=>time());
			}
		}
		$this->load->model(array('admin/Photo_model'));
		$result = $this->Photo_model->add_batch($data);
		is_AjaxResult($result);
	}
	
}
