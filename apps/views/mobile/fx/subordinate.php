<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header'); ?>
<div id="app">
	<van-tabs :active="active" sticky @click="getList"  class="my-order">
		<van-tab v-for="(v,i) in tree" :title="v" class="td_content" >
			<template v-if="list">
				<van-cell-group class="mt10" >
				  <van-cell v-for="(v,index) in list">
					  <van-col span="4"><img v-lazy="v.thumb" class="brus50 "></van-col>
					  <van-col span="20" class="pl10"> 
					  	<p v-text="v.nickname"></p>
					  	<p v-text="v.addtime"></p>
					  </van-col>
				  </van-cell>
				</van-cell-group>
			</template>
			<template v-else><p class="cr_hs2 text-center mt60"><i class="iconfont icon-meiyoujieguo f80 d_block"></i><span v-text="'什么也木有~'"></span></p></template>
		</van-tab>
	</van-tabs>
	<div class="mt60">&nbsp;</div>
</div>

<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		active:0,
		list:"",
		ptclick:[],
		tree:['一队','二队','三队']
	},
  	methods: {
  		getList(id){
  			if(!Array.isArray(this.ptclick[id])){
  				var l = this.$toast.loading({message: '加载中...'});
  	  	  	  	this.ptclick.push(id);
	  			axios.post('/mobile/fx/subordinate', Qs.stringify({id:id}),ajaxconfig).then((response)=> {
	  	  	  	  	var data = response.data;
	  	  	      	if(data.state==1){
		  	  	      	this.ptclick[id] = data.data;
	    	  	   		this.list = data.data;
	  	  	  	  	}else{
	  	  	  	  	  	this.ptclick[id] = [];
	  	  	  	  		this.list = "";
	  	  	  	  	}
		  	  	    l.clear();
	  	  	    });
  			}else{
  				this.list = this.ptclick[id].length?this.ptclick[id]:'';
  	  	    }
  	  	}
  	},
  	mounted:function (){
  		this.getList(this.active);
	}
});
</script>
</body>
</html>