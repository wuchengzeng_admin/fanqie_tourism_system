<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 百度编辑器上传图片 em 专用啊
 */
class Imageup extends CI_Controller {
	function index() {
		header("Content-Type:text/html;charset=utf-8");
		error_reporting ( E_ERROR | E_WARNING );
		date_default_timezone_set ( "Asia/chongqing" );
		// 上传配置
		$path = "/res/upload/umedit/{yyyy}{mm}{dd}/{time}{rand:6}";
		$config = array ("pathFormat" => $path,"maxSize" => 2000, "allowFiles" => array (".gif",".png",".jpg",".jpeg"));
		//背景保存在临时目录中
		$this->load->library('Uploader', array ('fileField' =>'upfile','config' => $config));
		$type = $_REQUEST ['type'];
		$callback = $_GET ['callback'];
		$info = $this->uploader->getFileInfo ();
		/**
		 * 返回数据
		*/
		if ($callback) {
			echo '<script>' . $callback . '(' . json_encode ( $info ) . ')</script>';
		} else {
			echo json_encode ( $info );
		}
		exit ();
	}
	
	function del(){
		if(is_ajax_request()){
			$thumb = Posts('thumb');
			$thumbs = substr($thumb,1);
			if (is_file($thumbs))unlink($thumbs);
		}
	}
	
	function ueditor() {
		header ( "Content-Type:text/html;charset=utf-8" );
		error_reporting ( E_ERROR | E_WARNING );
		date_default_timezone_set ( "Asia/chongqing" );
		//上传配置
		$config = array (
				"savePath" => "res/upload/baiduedit/", //存储文件夹
				"maxSize" => 2000, //允许的文件最大尺寸，单位KB
				"allowFiles" => array (
						".gif",
						".png",
						".jpg",
						".jpeg",
						".bmp" 
				)  //允许的文件格式
		);
		//上传文件目录
		$Path = "res/upload/baiduedit/";
		//背景保存在临时目录中
		$config["savePath"] = $Path;
		$this->load->library ( 'Uploader', array (
				'fileField' => 'upfile',
				'config' => $config,
				'base64' => false 
		) );
		$type = $_REQUEST ['type'];
		$callback = $_GET ['callback'];
		$info = $this->uploader->getFileInfo ();
		/**
		 * 返回数据
		 */
		if ($callback) {
			echo '<script>' . $callback . '(' . json_encode ( $info ) . ')</script>';
		} else {
			echo json_encode ( $info );
		}
		exit ();
	}
}
	
	
