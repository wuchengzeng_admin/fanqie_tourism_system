<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline ">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<form class="layui-form" method="post">
				<table class="layui-table">
					<thead>
						<tr>
							<th>ID</th>
							<th>名称</th>
							<th>粉丝量</th>
							<th>一级</th>
							<th>二级</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
                            <?php if (empty($items)){ ?>
                            <tr>
								<td class="empty-table-td"><?php echo $emptyRecord;?></td>
							</tr>
                            <?php  }else{foreach ($items as $key=>$v){ ?>
                            <tr>
                            <td><?php echo $v['id'];?></td>
							<td><?php echo $v['gname'];?></td>
							<td><?php echo $v['fans_num'];?></td>
							<td><?php echo $v['p_1'];?></td>
							<td><?php echo $v['p_2'];?></td>
							<td>
								<div class="layui-btn-group">
								  <?php echo admin_btn(site_url($dr_url.'/edit/id-'.$v['id']),'edit','layui-btn-xs');?>
								  <?php echo admin_btn(site_url($dr_url.'/del/id-'.$v['id']),'del','layui-btn-xs f_del');?>
								</div>
							</td>
							</tr>
                            <?php }}?>
                	</tbody>
				</table>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>