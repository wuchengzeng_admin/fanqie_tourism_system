<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
			<form class="layui-form" action="">
				<div class="layui-inline">
				    <div class="layui-input-inline">
				    	<input type="text" value="<?php echo $val;?>" name="val" placeholder="请输入姓名或者帐号" class="layui-input" lay-verify='required'>
				    </div>
				    <div class="layui-inline">
				    <?php echo admin_btn('', 'find',"")?>
				    </div>
				</div>
				<div class="layui-inline f-right">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
			</form>
		</blockquote>
		<form class="layui-form" method="post">
		  	<table class="layui-table">
			    <thead>
					<tr>
						<th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
						<th>角色名称</th>
						<th>角色说明</th>
						<th>创建时间</th>
						<th>操作</th>
					</tr> 
			    </thead>
			    <tbody>
                    <?php if (empty($items)){?>
                    <tr>
						<td class="empty-table-td"><?php echo $emptyRecord;?></td>
					</tr>
                    <?php }?>
					<?php foreach ($items as $k=>$v){ ?>
                     <tr id="list_<?php echo $v['id']?>">
                        <td>
                           <input type="checkbox" name="checked[<?php echo $k; ?>]" ckd="ckd" lay-skin="primary" value="<?php echo $v['id']?>" lay-filter="choose">
                        </td>
						<td><?php echo $v['name']; ?></td>
						<td><?php echo $v['summary']; ?></td>
						<td><?php echo format_time($v['add_time']); ?></td>
						<td>
						<div class="layui-btn-group">
						  <?php echo admin_btn(site_url("$dr_url/edit/id-".$v['id']),'edit','layui-btn-xs');?>
						  <?php echo admin_btn(site_url("$dr_url/del/id-".$v['id']),'del','layui-btn-xs f_del');?>
						  <?php echo admin_btn(site_url($dr_url.'/permission/id-'.$v['id']),'','layui-btn-xs layui-btn-warm','','添加权限');?>
						</div>
						</td>
					</tr>
                    <?php }?>
	             </tbody>
			</table>
		</form>
</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>