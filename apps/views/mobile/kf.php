<a href="javascript:void(0)" class="upload-btn p8 br-circle bg-diy " id="ct_kf">
	<svg class="icon" aria-hidden="true" color="#fff" font-size="0.25rem">
		<use xlink:href="#icon-kefu"></use>
	</svg>
</a>
<script type="text/javascript">
$(function(){
	$('#ct_kf').click(function(){
		 layer.open({
			    title: ['识别以下二维码联系客服']
			    ,content: '<img src="<?php echo admin_config_cache('wechat')['wechat_kf_qrcode'];?>">'
			  });
	});
});
</script>
